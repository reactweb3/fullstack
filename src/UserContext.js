import React from "react";

// Create a Context Object
const UserContext = React.createContext();

// The "Provider" component allows other components to consume/use the context
export const UserProvider = UserContext.Provider;

export default UserContext;

// UserContext.js
// import React, { createContext, useState } from 'react';

// const UserContext = createContext();

// const UserProvider = ({ children }) => {
//   const [user, setUser] = useState(null);

//   return (
//     <UserContext.Provider value={{ user, setUser }}>
//       {children}
//     </UserContext.Provider>
//   );
// };

// export { UserProvider, UserContext };
