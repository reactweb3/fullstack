import { useState, useEffect } from "react";
import axios from 'axios';
import { useParams, useNavigate } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css'; // Import Bootstrap CSS
import Navbar from './Navbar';

export default function ViewDetails() {
    const [product, setProduct] = useState(null);
    const [quantity, setQuantity] = useState(1);
    const { id } = useParams();
    const url = "https://backend-dog-7ts1.onrender.com";
    const navigate = useNavigate();

    useEffect(() => {
        if (!id) {
            navigate('/product'); // Redirect to products page if id is missing
            return;
        }

        const fetchData = async () => {
            try {
                const response = await axios.get(`${url}/products/${id}`);
                setProduct(response.data);
                // console.log(response);
            } catch (error) {
                console.error("Error: ", error);
                navigate('/product'); // Redirect to products page on error
            }
        };

        fetchData();
    }, [id, navigate]);

    const handleBuy = async () => {
        fetch(`${url}/orders/${id}/getToOrder`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                quantity: quantity
            })
        }).then(res => res.json()).then(data => {
            if(data) {
                alert('Successfully purchased!');
                navigate('/product');
            } else {
                alert("Failed to purchased order")
            }
        })
    };

    const minusQuantity = () => {
            if (quantity > 1) {
                setQuantity(prevQuantity => prevQuantity - 1);
            }
            if (quantity === 1) {
                alert('Minimum order number is 1');
            }
            return quantity;
    }
    
    const addQuantity = () => {
            if (quantity < product.stocks) {
                setQuantity(prevQuantity => prevQuantity + 1);
            }
            if (quantity === product.stocks) {
                alert('Insufficient stocks products');
            }
            return quantity;
    }

    if (!product) {
        return (
            <div>
                <h1 className="d-flex justify-content-center mt-4">Product View</h1>
                <h2 className="`d-flex justify-content-center` mt-4">Loading...</h2>
            </div>
        );
    }

    return (
        <div>
        <Navbar /> 
        <div className="px-4" style={{ background: 'gray'}}>
            <h1 className="d-flex justify-content-center mt-4 mb-5 fw-bold">Dog details</h1>
            <div className="row mb-5">
                <div className="col-md-6 mb-5">
                    <label htmlFor="name" className="text-white fw-bold">Name:</label>
                    <input type="text" id="name" className="form-control" value={product.name} readOnly />
                </div>
                <div className="col-md-6 mb-5">
                    <label htmlFor="description" className="text-white fw-bold">Description:</label>
                    <input type="text" id="description" className="form-control" value={product.description} readOnly />
                </div>
                <div className="col-md-6 mb-5">
                    <label htmlFor="price" className="text-white fw-bold">Price:</label>
                    <input type="text" id="price" className="form-control" value={product.price} readOnly />
                </div>
                <div className="col-md-6 mb-5">
                    <label htmlFor="stocks" className="text-white fw-bold">Stocks:</label>
                    <input type="text" id="stocks" className="form-control" value={product.stocks} readOnly />
                </div>
                <div className="d-flex justify-content-center col-12 w-100 border p-5 bg-black image_view">
                    <img src={product.image} alt={product.name} style={{ maxWidth: '100%', height: 'auto' }} />
                </div>
            </div>
            <div className="row">
                <div className="col-md-12 d-flex justify-content-center p-5">
                    <div className="px-4">
                        <button className="btn btn-secondary" onClick={minusQuantity}>-</button>
                    </div>
                    <div className="ds-quan d-flex justify-content-center py-2 border rounded-1 bg-white">
                        <span className="">{quantity}</span>
                    </div>
                    <div className="px-4">
                        <button className="btn btn-secondary" onClick={addQuantity}>+</button>
                    </div>
                </div>
            </div>
            <div className="row px-5">
                <div className="col-md-12 d-flex justify-content-center mb-3">
                    <div className="me-3">
                    <button className="btn btn-success" onClick={() => navigate('/product')}>Back</button>
                    </div>
                    <div className="">
                       <button className="btn btn-primary ml-2" onClick={handleBuy}>Buy</button>
                    </div>
                </div>
            </div>
        </div>   
    </div>
    );
}

