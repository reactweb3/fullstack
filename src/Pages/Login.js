import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'; // Import Bootstrap CSS
import axios from 'axios';
import { Navigate } from 'react-router-dom';
import swal from 'sweetalert';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const [isLoggedIn, setIsLoggedIn] = useState(false); // Track login status
  const [user, setUser] = useState(null); // Define user state
  const url = "https://backend-dog-7ts1.onrender.com"

  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${url}/users/login`, { email, password });
      const { access } = response.data;
      if(access === undefined) {
        swal({
          title: "Error！",
          text: "The email address or password is incorrect.",
          icon: "warning",
          buttons: {
              confirm: {
                  text: "Confirm",
                  className: "custom-swal-button" // Apply the custom class to the button
              },
          }
      });
      return;
      } else {
        localStorage.setItem('accessToken', access);
        setIsLoggedIn(true);
        retrieveUserDetails(access); // Call retrieveUserDetails with the token
      }
    } catch (error) {
      if (error.response) {
        console.error('Server responded with an error:', error.response.data);
      } else if (error.request) {
        console.error('No response received:', error.request);
      } else {
        console.error('Error setting up the request:', error.message);
      }
    }
  };

  const retrieveUserDetails = (token) => {
  fetch(`${url}/users/details`, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({}) // If no additional data is needed in the request body
  })
  .then(res => res.json())
  .then(data => {
    // console.log('User Data:', data);
    localStorage.setItem('id', data._id);
    localStorage.setItem('user_type', data.isAdmin);
    localStorage.setItem('name', data.firstName);
    localStorage.setItem('last_name', data.lastName);
    localStorage.setItem('email', data.email);
    // Update user state with retrieved data
    setUser({
      id: data._id,
      isAdmin: data.isAdmin,
      username: data.username
    });
  })
  .catch(error => {
    console.error('Error retrieving user details:', error);
    // Handle error as needed
  });
};

  if (isLoggedIn) {
    return <Navigate to="/product" />;
  }

  return (
    <div className="section p-5">
      <div className="container con_desing">
        <h1 className="d-flex justify-content-center mt-3">Login</h1>
        <Form onSubmit={handleLogin}>
          <Form.Group controlId="formBasicEmail">
            <Form.Label className='fw-bold'>Email address:</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group controlId="formBasicPassword">
            <Form.Label className='fw-bold'>Password:</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>
          <div className="d-flex justify-content-center mt-4">
            <Button variant="primary" type="submit" id="btn_login" className='fw-bold'>
              Login
            </Button> 
          </div>
          <div className="d-flex justify-content-center mt-3">
            <a href="/register">Don't have an account? Register here</a>
          </div>
          {error && <div className="error-message">{error}</div>}
        </Form>
      </div>
    </div>
  );
}
