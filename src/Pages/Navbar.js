import React, { useContext, useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import { GiHamburgerMenu } from "react-icons/gi";
import '../App.css';

export default function Navbar() {
  const { unsetUser, setUser } = useContext(UserContext);
  const navigate = useNavigate();
  const [show, setShow] = useState(false);

  const handleLogout = () => {
    unsetUser();
    setUser({
      id: null,
      isAdmin: null,
      username: null,
      token: null
    });
    localStorage.removeItem('accessToken');
    navigate('/login');
  };

  const user_type = localStorage.getItem('user_type');
  const name = localStorage.getItem('name');
  const l_name = localStorage.getItem('last_name');

  return (
    <div className="landing">
      <div className="headburger row align-items-center">
        <div className="px-4 col-6">
          <header className="f_bg">
            <GiHamburgerMenu className="hamburger-icon" onClick={() => setShow(!show)} />
          </header>
        </div>
        <div className="col-6 d-flex justify-content-end px-4 align-items-center" onClick={() => navigate('/profile')}>
          <h1 className="mx-2 text-white">
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" className="bi bi-person-circle" viewBox="0 0 16 16">
              <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0" />
              <path fillRule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8m8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1" />
            </svg>
          </h1>
          <p className="fw-bold my-auto text-white">
            {name} {l_name}
          </p>
        </div>
      </div>

      <div className={`offcanvas offcanvas-start ${show ? 'show' : ''}`} style={{ transform: show ? 'translateX(0)' : 'translateX(-100%)' }}>
        <div className="offcanvas-header">
          <h5 className="offcanvas-title"></h5>
          <button type="button" className="btn-close" aria-label="Close" onClick={() => setShow(false)}></button>
        </div>
        <div className="offcanvas-body">
          <ul className="navbar-nav">
            <li className="nav-item navbar_des border rounded-3 px-1">
              <NavLink to="/product" className="nav-link fw-bold" onClick={() => setShow(false)}>Home</NavLink>
            </li>
            <li className="nav-item navbar_des border rounded-3 px-1 mt-2">
              <NavLink to="/profile" className="nav-link fw-bold" onClick={() => setShow(false)}>Profile</NavLink>
            </li>
            {user_type === "true" && (
              <>
                <li className="nav-item navbar_des border rounded-3 px-1 mt-2">
                  <NavLink to="/allproducts" className="nav-link fw-bold" onClick={() => setShow(false)}>All Products</NavLink>
                </li>
                <li className="nav-item navbar_des border rounded-3 px-1 mt-2">
                  <NavLink to="/allOrders" className="nav-link fw-bold" onClick={() => setShow(false)}>User Orders</NavLink>
                </li>
              </>
            )}
            {user_type === "false" && (
              <li className="nav-item navbar_des border rounded-3 px-1 mt-2">
                <NavLink to="/orders" className="nav-link fw-bold" onClick={() => setShow(false)}> Order</NavLink>
              </li>
            )}
            <li className="nav-item navbar_des border rounded-3 px-1 py-2 mt-2" onClick={handleLogout}>
              <button className="nav-link btn btn-link fw-bold" style={{ padding: 0 }}>
                Logout
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
