import { useState, useEffect } from "react";
import axios from "axios";
import { useParams, useNavigate } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css'; // Import Bootstrap CSS
import Navbar from './Navbar';
import { SiAnsys } from "react-icons/si";

export default function AllOrders() {
    const url = "https://backend-dog-7ts1.onrender.com";
    const token = localStorage.getItem('accessToken');
    const [allord, setAllord] = useState(null);

    useEffect(() => {
        getAllOrder();
    }, []);
    
    const getAllOrder = () => {
        if(!token) {
            console.log("to token found");
        }

        fetch(`${url}/orders/allOrders`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setAllord(data);
        })
        .catch(error => {
            console.error('Error retrieving order details:', error);
        });
    }

    return (
        <div className="bg_order">
            <Navbar />
            <div className="container mt-4">
                <div className="d-flex justify-content-center">
                    <h1 className="mb-4">List of user orders</h1>
                </div>
                <div className="table-responsive">
                    <table className="table table-striped table-hover bg-white">
                        <thead className="table-secondary">
                            <tr>
                                {/* <th>Order ID</th> */}
                                <th>Customer ID</th>
                                <td>Dog Name</td>
                                <th>Dog Price</th>
                                <th>Quantity</th>
                                <th>Total Price</th>
                                <th>Date Order</th>
                            </tr>
                        </thead>
                        <tbody className="table table-striped table-hover">
                           {allord && allord.length > 0 ? (
                            allord.map((allordItem, allordIndex) => (
                                <tr key={allordIndex}>
                                    <td>{allordItem._id}</td>
                                    <td>{allordItem.product[0]?.name}</td>
                                    <td>{allordItem.product[0]?.price}</td>
                                    <td>{allordItem.product[0]?.quantity}</td>
                                    <td>{allordItem.subTotal}</td>
                                    <td>{new Date(allordItem.addedOn).toLocaleDateString()}</td>
                                </tr>
                            ))
                            
                           ) : (
                            <tr>
                                <td colSpan="8" className="text-center">No orders found</td>
                            </tr>
                           )

                           }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}