import { useState, useEffect } from "react";
import axios from 'axios';
import { useParams, useNavigate } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css'; // Import Bootstrap CSS
import Navbar from './Navbar';
import { SiAnsys } from "react-icons/si";


export default function ViewOrder() {
    const url = "https://backend-dog-7ts1.onrender.com";
    const token = localStorage.getItem('accessToken');
    const user_id = localStorage.getItem('id');
    const [order, setOrder] = useState(null); 

    useEffect(() => {
        getOrder();
      }, []);
      
      const getOrder = () => {
        if (!token) {
          alert('No token found. Please log in.');
          return;
        }
        
        fetch(`${url}/orders/myOrders/${user_id}`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
          }
        })
        .then(res => res.json())
        .then(data => {
          console.log('Order Data:', data);
          setOrder(data);
        })
        .catch(error => {
          console.error('Error retrieving order details:', error);
          // Handle error as needed
        });
      };

    return (
        <div className="bg_order">
            <Navbar />
            <div className="container mt-4">
                <div className="d-flex justify-content-center">
                    <h1 className="mb-4">List of your orders</h1>
                </div>
                <div className="table-responsive">
                    <table className="table table-striped table-hover bg-white">
                        <thead className="table-secondary">
                            <tr>
                                <th>Order ID</th>
                                <th>Name</th>
                                <th>Total price</th>
                                <th>Quantity</th>
                                <th>Date order</th>
                            </tr>
                        </thead>
                        <tbody className="table table-striped table-hover">
                        {order && order.length > 0 ? (
                                order.map((orderItem, orderIndex) => (
                                    <tr key={orderIndex}>
                                        <td>{orderItem._id}</td>
                                        <td>{orderItem.product[0]?.name}</td>
                                        <td>${orderItem.subTotal}</td>
                                        <td>{orderItem.product[0]?.quantity}</td>
                                        <td>{new Date(orderItem.addedOn).toLocaleDateString()}</td>
                                    </tr>
                                ))
                            ) : (
                                <tr>
                                    <td colSpan="5" className="text-center">No orders found</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}