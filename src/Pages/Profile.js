import { useState, useEffect } from "react";
import axios from 'axios';
import { Form, Button } from 'react-bootstrap';
import { useParams, useNavigate } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css'; // Import Bootstrap CSS
import Navbar from './Navbar';


export default function Profile() {

  const url = "https://backend-dog-7ts1.onrender.com";
  const [firstName, setfirstName] = useState('');
  const [lastName, setlastName] = useState('');
  const [email, setEmail] = useState('');
  const [type, setType] = useState('');
  const [createdOn, setCreatedOn] = useState('');
  // const [password, setPassword] = useState('');
  const token = localStorage.getItem('accessToken');

  useEffect(() => {
    userDetails(token);
  }, []);
  
  const userDetails = (token) => {
    if (!token) {
      alert('No token found. Please log in.');
      return;
    }
    
    fetch(`${url}/users/details`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({}) // If no additional data is needed in the request body
    })
    .then(res => res.json())
    .then(data => {
      // console.log('User Data:', data);
      // const data = response.data;
      setfirstName(data.firstName);
      setlastName(data.lastName);
      setEmail(data.email);
      setType(data.isAdmin ? 'Admin' : 'Customer');
      // setPassword(data.password);
      const date = new Date(data.createdOn);
      const formattedDate = date.toLocaleDateString('en-CA'); // yyyy-mm-dd format
      setCreatedOn(formattedDate.replace(/-/g, '/'));
    })
    .catch(error => {
      console.error('Error retrieving user details:', error);
      // Handle error as needed
    });
  };
  // console.log(password)

    return (
      <div className="bg_profile">
        <Navbar />
        <div className="d-flex justify-content-center p-5">
          <div className="container m-3 bord">
            <h1 className="text-center">Profile</h1>
            <div>
              <label htmlFor="name" className="fw-bold">First Name:</label>
              <input type="text" id="name" className="form-control" value={firstName} onChange={(e) => setfirstName(e.target.value)} />
              <label htmlFor="name" className="fw-bold mt-2">Last Name:</label>
              <input type="text" id="name" className="form-control" value={lastName} onChange={(e) => setlastName(e.target.value)}/>
            </div>
            <div>
              <label htmlFor="name" className="fw-bold mt-2">Email:</label>
              <input type="text" id="name" className="form-control" value={email} onChange={(e) => setEmail(e.target.value)}/>
              <label htmlFor="name" className="fw-bold mt-2">Type of user:</label>
              <input type="text" id="name" className="form-control" value={type} disabled/>
              <label htmlFor="name" className="fw-bold mt-2">Create date:</label>
              <input type="text" id="name" className="form-control" value={createdOn} disabled/>
            </div>
            <div className="d-flex justify-content-center mt-3">
              <Button variant="primary" type="submit" id="btn_login" className='fw-bold'>
                Save
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }