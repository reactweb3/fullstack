import { useState, useEffect } from "react";
import axios from 'axios';
import { useParams, useNavigate } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css'; // Import Bootstrap CSS
import Navbar from './Navbar';

export default function EditView() {
    const [product, setProduct] = useState(null);
    const { id } = useParams();
    const url = "https://backend-dog-7ts1.onrender.com";
    // const [ image, setImage ] = useState('');

    const navigate = useNavigate();

    useEffect(() => {
        if (!id) {
            navigate('/product'); // Redirect to products page if id is missing
            return;
        }

        const fetchData = async () => {
            try {
                const response = await axios.get(`${url}/products/${id}`);
                setProduct(response.data);
                console.log(response);
            } catch (error) {
                console.error("Error: ", error);
                navigate('/product'); // Redirect to products page on error
            }
        };

        fetchData();
    }, [id, navigate]);

    if (!product) {
        return (
            <div>
                <h1 className="d-flex justify-content-center mt-4">Product View</h1>
                <h2 className="`d-flex justify-content-center` mt-4">Loading...</h2>
            </div>
        );
    }

    return (
        <div>
            <Navbar /> 
            <div className="px-4">
                <h1 className="d-flex justify-content-center mt-4 mb-5">Product View</h1>
                <div className="row mb-5">
                    <div className="col-md-6 mb-5">
                        <label htmlFor="name">Name: </label>
                        <input type="text" id="name" value={product.name} readOnly />
                    </div>
                    <div className="col-md-6 mb-5">
                        <label htmlFor="description">Description: </label>
                        <input type="text" id="description" value={product.description} readOnly />
                    </div>
                    <div className="col-md-6 mb-5">
                        <label htmlFor="price">Price: </label>
                        <input type="text" id="price" value={product.price} readOnly />
                    </div>
                    <div className="col-md-6 mb-5">
                        <label htmlFor="stocks">Stocks: </label>
                        <input type="text" id="stocks" value={product.stocks} readOnly />
                    </div>
                    <div className="d-flex justify-content-center col-12 w-100">
                        <img src={product.image} alt={product.name} />
                    </div>
                </div>
                <div className="row px-5">
                    <div className="col-md-6 d-flex justify-content-end mb-3">
                        <button className="btn btn-primary">Buy</button>
                    </div>
                    <div className="col-md-6">
                    <button className="btn btn-success" onClick={() => navigate('/product')}>Go Back</button>
                    </div>
                </div>
            </div>   
        </div>
    );
}