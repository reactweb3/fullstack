import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import { Card } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'; // Import Bootstrap CSS
import Navbar from './Navbar';
import swal from 'sweetalert';

export default function Products() {
    const [products, setProducts] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [image, setImage] = useState('');
    const [price, setPrice] = useState('');
    const [stocks, setStocks] = useState('');
    const [isActive, setIsActive] = useState(true); // Add state for isActive
    const [currentPage, setCurrentPage] = useState(1);
    const [productsPerPage] = useState(4); // Products per page
    const [searchTerm, setSearchTerm] = useState(""); // State to store search term
    const url = "https://backend-dog-7ts1.onrender.com"


    
    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const token = localStorage.getItem('accessToken');
        if (!token) {
            console.error('No token found. Please log in.');
            return;
        }

        try {
            const response = await axios.get(`${url}/products/all`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            setProducts(response.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const openModal = () => {
        setShowModal(true);
    };

    const closeModal = () => {
        setShowModal(false);
        resetForm();
    };

    const resetForm = () => {
        setName('');
        setDescription('');
        setImage('');
        setPrice('');
        setStocks('');
        setIsActive(true); // Reset isActive state
    }

    const addProduct = async (e) => {
        e.preventDefault(); // Prevent default form submission
        const token = localStorage.getItem('accessToken'); // Retrieve token from local storage

        if (!token) {
            console.error('No token found. Please log in.');
            return;
        }
        if(name == "" || description == "" || image == "" || price == "" || stocks == "" ){
            swal({
                title: "Error!",
                text: "Please fill in all the fields",
                icon: "warning",
                buttons: {
                    confirm: {
                        text: "Confirm",
                        className: "custom-swal-button" // Apply the custom class to the button
                    },
                }
            });
            return;
        }
        // console.log('Token:', token); // Log the token

        try {
            const response = await axios.post(`${url}/products/add`, {
                name,
                description,
                image,
                price,
                stocks,
                isActive
            }, {
                headers: {
                    'Authorization': `Bearer ${token}` // Add Bearer prefix
                }
            });
            console.log('Response:', response.data); // Log the response data
            swal({
                title: "Success!",
                text: "Product added successfully",
                icon: "success",
                buttons: {
                    confirm: {
                        text: "Confirm",
                        className: "custom-swal-button" // Apply the custom class to the button
                    },
                }
            });
            fetchData();
            closeModal();
        } catch (error) {
            console.error('Error adding product:', error.response?.data?.error || error.message);
            // setError(error.response?.data?.error || error.message); // Set error message in state
        }
    };

    // Pagination logic
    const activeProducts = products.filter(product => product.isActive); // Only active products
    const indexOfLastProduct = currentPage * productsPerPage;
    const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
    const currentProducts = activeProducts.slice(indexOfFirstProduct, indexOfLastProduct); // Use activeProducts here
    const totalPages = Math.ceil(activeProducts.length / productsPerPage); // Only count active products
    // console.log(productsPerPage);
    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    const user_type = localStorage.getItem('user_type');

    // search function
    const search = () => {
        const searchValue = document.getElementById("dog_name").value;
        
        if(searchValue === "") {
            alert("Fill in the search fields.")
            fetchData();
            return;
        }

        // Create filtered products using the searchValue directly instead of depending on searchTerm state
        const filteredProducts = products.filter(
            (product) => 
                product.isActive && (
                    product.name.toLowerCase().includes(searchValue.toLowerCase()) ||
                    product.description.toLowerCase().includes(searchValue.toLowerCase())
                )
        );

        if(filteredProducts.length === 0) {
            alert("There no dog found.")
            fetchData();
            return;
        }

        setSearchTerm(searchValue);
        setProducts(filteredProducts);
        const totalFilteredPages = Math.ceil(filteredProducts.length / productsPerPage);
        if (currentPage > totalFilteredPages) {
            setCurrentPage(1);
        }
    };

    const clearInput = () => {
        document.getElementById("dog_name").value = "";
        setSearchTerm("");
        fetchData();
    }

    return (
        <div className="products_bg">
            <Navbar />
            <div className="mx-5 mt-4">
                <div className="d-flex justify-content-end">
                    <input type="text" className="int_dog mx-2" id="dog_name" placeholder="Dog Name" />
                    <button className="btn btn-dark mx-2" onClick={search}>Search</button>
                    <button className="btn btn-dark mx-2" onClick={clearInput}>Clear</button>
                </div>
            </div>
            <div className="row py-5 px-5 validate">
            {currentProducts.map(product => ( // Display only currentProducts (filtered active products)
                <div key={product._id} className="col-md-3">
                    <div className="mb-3" style={{ width: '100%' }}>
                        <Card style={{ width: '280px', height: '500px', boxShadow: '0 0 10px 0 rgba(0, 0, 0, 0.6)' }}>
                            <Card.Img className="img_product" variant="top" src={product.image}/>
                            <Card.Body className='body_card'>
                                <Card.Title className="fw-bold">Dog name: {product.name}</Card.Title>
                                <Card.Text>Description: {product.description}</Card.Text>
                                <Card.Text>Price: ${product.price}</Card.Text>
                                <Card.Text>Stocks: {product.stocks}</Card.Text>
                                <Link to={`/details/${product._id}`} className="btn btn-primary">Details</Link>
                            </Card.Body>
                        </Card>
                    </div> 
                </div>
                ))} 
            </div>
            <div className="d-flex justify-content-end px-5 mx-5">
                <nav>
                    <ul className="pagination">
                        {Array.from({ length: totalPages }, (_, index) => (
                            <li key={index + 1} className={`page-item ${currentPage === index + 1 ? 'active' : ''}`}>
                                <button onClick={() => paginate(index + 1)} className="page-link">
                                    {index + 1}
                                </button>
                            </li>
                        ))}
                    </ul>
                </nav>
            </div>
            <div>
                {user_type == "true" && (
                <div className="d-flex justify-content-end px-4 added" id="adding">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-plus-square-fill" viewBox="0 0 16 16" onClick={openModal}>
                    <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0"/>
                    </svg>
                </div>
                )}
            </div>
            <div className={`modal ${showModal ? 'show' : ''}`} tabIndex="-1" style={{ display: showModal ? 'block' : 'none' }}>
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create new products</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={closeModal}></button>
                </div>
                <form onSubmit={addProduct}>
                    <div className="modal-body">
                        <div className="mb-3">
                            <label htmlFor="name">Name:</label>
                            <input type="text" id="name" name="name" value={name} onChange={(e) => setName(e.target.value)} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description">Description:</label>
                            <input type="text" id="description" name="description" value={description} onChange={(e) => setDescription(e.target.value)} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="image">Image:</label>
                            <input type="text" id="image" name="image" value={image} onChange={(e) => setImage(e.target.value)} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="price">Price:</label>
                            <input type="text" id="price" name="price" value={price} onChange={(e) => setPrice(e.target.value)} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="stocks">Stocks:</label>
                            <input type="text" id="stocks" name="stocks" value={stocks} onChange={(e) => setStocks(e.target.value)} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="isActive">Active:</label>
                            <input type="checkbox" id="isActive" name="isActive" checked={isActive} onChange={(e) => setIsActive(e.target.checked)} />
                        </div>
                    </div>
                    <div className="modal-footer d-flex justify-content-center">
                        <button type="submit" className="btn btn-success">New product</button>
                    </div>
                </form>
                </div>
            </div>
            </div>
        </div>
    );

}
