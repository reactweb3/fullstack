import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Navbar from './Navbar'; // Assuming Navbar is a separate component
import swal from 'sweetalert';

export default function Products() {
    const [products, setProducts] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [image, setImage] = useState('');
    const [price, setPrice] = useState('');
    const [stocks, setStocks] = useState('');
    const [isActive, setIsActive] = useState(true);
    const [editProduct, setEditProduct] = useState(null);
    const [showEditModal, setShowEditModal] = useState(false);
    const url = "https://backend-dog-7ts1.onrender.com";

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const token = localStorage.getItem('accessToken');
        if (!token) {
            console.error('No token found. Please log in.');
            return;
        }

        try {
            const response = await axios.get(`${url}/products/all`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            setProducts(response.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const openModal = () => {
        setShowModal(true);
    };

    const closeModal = () => {
        setShowModal(false);
        resetForm();
    };

    const openEditModal = (product) => {
        setEditProduct(product);
        setName(product.name);
        setDescription(product.description);
        setImage(product.image);
        setPrice(product.price);
        setStocks(product.stocks);
        setIsActive(product.isActive);
        setShowEditModal(true);
    };

    const closeEditModal = () => {
        setShowEditModal(false);
        resetForm();
    };

    const resetForm = () => {
        setName('');
        setDescription('');
        setImage('');
        setPrice('');
        setStocks('');
        setIsActive(true);
    };

    const updateProduct = async (e) => {
        e.preventDefault();
        const token = localStorage.getItem('accessToken');

        if (!token) {
            console.error('No token found. Please log in.');
            return;
        }

        try {
            const response = await axios.put(`${url}/products/update/${editProduct._id}`, {
                name,
                description,
                image,
                price,
                stocks
            }, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            console.log('Response:', response.data);
            swal({
                title: "Success!",
                text: "Product updated successfully",
                icon: "success",
                buttons: {
                    confirm: {
                        text: "Confirm",
                        className: "custom-swal-button" // Apply the custom class to the button
                    },
                }
            });
            // alert("Product updated successfully");
            fetchData();
            closeEditModal();
        } catch (error) {
            console.error('Error updating product:', error.response?.data?.error || error.message);
        }
    };

    const deleteProduct = async (productId) => {
        const token = localStorage.getItem('accessToken');

        if (!token) {
            console.error('No token found. Please log in.');
            return;
        }

        try {
            await axios.post(`${url}/products/deactive/${productId}`, {}, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

            // Assuming successful deactivation, now delete the product from the UI
            setProducts(products.map(product => {
                if (product._id === productId) {
                    return { product, isActive: !product.isActive }; // Toggle isActive status
                }
                return product;
            }));
            fetchData();
            swal({
                title: "Success!",
                text: "Product successfully update status",
                icon: "success",
                buttons: {
                    confirm: {
                        text: "Confirm",
                        className: "custom-swal-button" // Apply the custom class to the button
                    },
                }
            });
            // alert("Product Update successfully");
        } catch (error) {
            console.error('Error deleting product:', error.response?.data?.error || error.message);
        }
    };

    return (
        <div className='bg_allproduct'>
            <Navbar />
            <div className="row py-5 px-5">
                {products.map(product => (
                    <div key={product._id} className="col-md-3">
                        <div className="mb-3" style={{ width: '100%' }}>
                            <Card style={{ width: '280px', height: '500px', boxShadow: '0 0 10px 0 rgba(0, 0, 0, 0.6)' }}>
                                <Card.Img variant="top" className='all_img' src={product.image} />
                                <Card.Body>
                                    <Card.Title className='fw-bold'>Dog name: {product.name}</Card.Title>
                                    <Card.Text>Description: {product.description}</Card.Text>
                                    <Card.Text>Price: ${product.price}</Card.Text>
                                    <Card.Text>Stocks: {product.stocks}</Card.Text>
                                    <div className="d-flex justify-content-between">
                                        <button className="btn btn-primary btn_edt" onClick={() => openEditModal(product)}>Edit</button>
                                        {product.isActive ? (
                                            <button className="btn btn-danger" onClick={() => deleteProduct(product._id)}>Deactivate</button>
                                        ) : (
                                            <button className="btn btn-success" onClick={() => deleteProduct(product._id)}>Reactivate</button>
                                        )}
                                    </div>
                                </Card.Body>
                            </Card>
                        </div>
                    </div>
                ))}
            </div>
            <div>
                <div className="d-flex justify-content-end px-4">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" className="bi bi-plus-square-fill" viewBox="0 0 16 16" onClick={openModal}>
                    </svg>
                </div>
            </div>
            <div className={`modal ${showModal ? 'show' : ''}`} tabIndex="-1" style={{ display: showModal ? 'block' : 'none' }}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Add New Product</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={closeModal}></button>
                        </div>
                        <form>
                            <div className="modal-body">
                                <div className="mb-3">
                                    <label htmlFor="name">Name:</label>
                                    <input type="text" id="name" name="name" value={name} onChange={(e) => setName(e.target.value)} />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="description">Description:</label>
                                    <input type="text" id="description" name="description" value={description} onChange={(e) => setDescription(e.target.value)} />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="image">Image:</label>
                                    <input type="text" id="image" name="image" value={image} onChange={(e) => setImage(e.target.value)} />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="price">Price:</label>
                                    <input type="text" id="price" name="price" value={price} onChange={(e) => setPrice(e.target.value)} />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="stocks">Stocks:</label>
                                    <input type="text" id="stocks" name="stocks" value={stocks} onChange={(e) => setStocks(e.target.value)} />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="isActive">Active:</label>
                                    <input type="checkbox" id="isActive" name="isActive" checked={isActive} onChange={(e) => setIsActive(e.target.checked)} />
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="submit" className="btn btn-primary">ADD</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div className={`modal ${showEditModal ? 'show' : ''}`} tabIndex="-1" style={{ display: showEditModal ? 'block' : 'none' }}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Edit Product</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={closeEditModal}></button>
                        </div>
                        <form onSubmit={updateProduct}>
                            <div className="modal-body">
                                <div className="mb-3">
                                    <label htmlFor="name">Name:</label>
                                    <input type="text" id="name" name="name" value={name} onChange={(e) => setName(e.target.value)} />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="description">Description:</label>
                                    <input type="text" id="description" name="description" value={description} onChange={(e) => setDescription(e.target.value)} />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="image">Image:</label>
                                    <input type="text" id="image" name="image" value={image} onChange={(e) => setImage(e.target.value)} />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="price">Price:</label>
                                    <input type="text" id="price" name="price" value={price} onChange={(e) => setPrice(e.target.value)} />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="stocks">Stocks:</label>
                                    <input type="text" id="stocks" name="stocks" value={stocks} onChange={(e) => setStocks(e.target.value)} />
                                </div>
                            </div>
                            <div className="modal-footer d-flex justify-content-center">
                                <button type="submit" className="btn btn-success">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}
