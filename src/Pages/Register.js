import { Form, Button } from 'react-bootstrap';
import { useState, useContext } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; // Import Bootstrap CSS
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import UserContext from "../UserContext";
import { Link } from 'react-router-dom';
import swal from 'sweetalert';
import '../App.css';

export default function Register(){
	const navigate = useNavigate();
	const { unsetUser, setUser } = useContext(UserContext);
	const [name, setName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const url = "https://backend-dog-7ts1.onrender.com";

	const createUser = async (e) => {
		e.preventDefault();
		
		if(name === '' || lastName === '' || email === '' || password === '') {
			alert("Please fill in all fields");
			return;
		}

		try {
			const response = await axios.post(`${url}/users/register`, { 
				firstName: name,
				lastName: lastName,
				email: email,
				password: password
			});
			
			if(response.data === true) {
				setName('');
				setLastName('');
				setEmail('');
				setPassword('');
				swal({
					title: "Success!",
					text: "User created successfully",
					icon: "success",
					buttons: {
						confirm: {
							text: "Confirm",
							className: "custom-swal-button" // Apply the custom class to the button
						},
					}
				});
				navigate('/login');
			} else {
				swal({
					title: "Error!",
					text: "User creation failed",
					icon: "warning",
					buttons: {
						confirm: {
							text: "Confirm",
							className: "custom-swal-button" // Apply the custom class to the button
						},
					}
				});
			}
		} catch (error) {
			console.error("There was an error creating the user!", error);
			alert("User creation failed");
		}
	}

	return (
	<div className='regis_bg px-5 d-flex align-items-center justify-content-center'>
		<div className='row re_border'>
			<div className='col-md-6 word-wrap px-5 py-5'>
				<h1 className='text-center mt-4 text-white'>Register now to view all available dogs!</h1>
				<p className='text-center text-white'>Welcome to our dog-selling website, where you can find any breed of dog you desire. Register now to explore our full selection and find your perfect furry companion!</p>
				<h2 className='text-center text-white'>Why Register?</h2>
				<ul>
					<li className='text-white'>Access to detailed profiles of all available dogs.</li>
					<li className='text-white'>Receive updates on new arrivals and special offers.</li>
					<li className='text-white'>Join a community of dog lovers and share your experiences.</li>
				</ul>
				<p className='text-white'>Don't wait! Register today and take the first step towards bringing home your new best friend.</p>
			</div>
			<div className='col-md-6 word-reg px-5 py-5'>
				<h1 className='text-center mt-4'>Register</h1>
				<Form onSubmit={createUser}>
					<Form.Group controlId="formFirstName">
						<Form.Label className='fw-bold'>First Name:</Form.Label>
						<Form.Control
						type="text"
						placeholder="Enter first name"
						value={name}
						onChange={(e) => setName(e.target.value)}
						required
						/>
					</Form.Group>
					<Form.Group controlId="formLastName">
						<Form.Label className='fw-bold mt-2'>Last Name:</Form.Label>
						<Form.Control
						type="text"
						placeholder="Enter last name"
						value={lastName}
						onChange={(e) => setLastName(e.target.value)}
						required
						/>
					</Form.Group>
					<Form.Group controlId="formBasicEmail">
						<Form.Label className='fw-bold mt-2'>Email address:</Form.Label>
						<Form.Control
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={(e) => setEmail(e.target.value)}
						required
						/>
					</Form.Group>
					<Form.Group controlId="formBasicPassword">
						<Form.Label className='fw-bold mt-2'>Password:</Form.Label>
						<Form.Control
						type="password"
						placeholder="Enter password"
						value={password}
						onChange={(e) => setPassword(e.target.value)}
						required
						/>
					</Form.Group>
					<div className='d-flex justify-content-center'>
						<Button variant="success" type="submit" id="reg_btn" className='mt-4 fw-bold'>Submit</Button>
					</div>
				</Form>
				<div className='d-flex flex-column align-items-center justify-content-center'>
					<p className='mt-3 fw-bold'>Already have an account? <Link to={"/login"}>Login Here!</Link></p>
				</div>
			</div>
		</div>
	</div>
	);
}
