import './App.css';
import Login from './Pages/Login.js';
import Register from './Pages/Register';
import Product from './Pages/Product';
import Navbar from './Pages/Navbar';
import Logout from './Pages/Logout';
import Profile from './Pages/Profile';
import Details from './Pages/ViewProduct';
import AllProducts from './Pages/Allproduct';
import Orders from './Pages/Orderproduct';
import Allorders from './Pages/Allorders.js'; 
import Edit from './Pages/Edit';
import React, { useState, useEffect } from 'react';
import { Route, Routes } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Container } from "react-bootstrap";
import { UserProvider } from "./UserContext";

function App() {
  const url = "https://backend-dog-7ts1.onrender.com"
  const [user, setUser] = useState({
    id: null,
    isAdmin: true,  //isAdmin: null,
    email: null,
    token: localStorage.getItem("accessToken")    
  });

  const unsetUser = () =>{
    localStorage.clear(); 
  };

  useEffect(()=>{
    // console.log(localStorage);
  }, [user]);

  useEffect(()=>{
    const token = localStorage.getItem('accessToken');
    // console.log('Token in localStorage:', token);
    if (token) {
      fetch(`${url}/users/details`, {
        headers:{
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {
        // console.log(data);

        if(data._id !== undefined){
          // console.log("Token fetched successfully");
          setUser({
              id: data._id,
              firstName : data.firstName,
              lastName : data.lastName,
              email : data.email,
              isAdmin: data.isAdmin,
              token: token // Set token in state
          });
        }
        else{
          // console.log("Token fetch failed");
          setUser({
            id: null,
            isAdmin: null,
            email: null,
            token: null // Clear token in state
          });
        }
      })
      .catch(error => {
        console.error("Error fetching user details:", error);
        // Handle error as needed
      });
    } else {
      setUser({
        id: null,
        isAdmin: null,
        email: null,
        token: null // Clear token in state
      });
    }
  }, []); // Empty dependency array means it runs once on component mount

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <div style={{ background: 'gray'}}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register/>} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/landing" element={<Navbar/>} />
            <Route path="/product" element={<Product/>} />
            <Route path="/allproducts" element={<AllProducts/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/details/:id" element={<Details/>} />
            <Route path="/editProduct" element={<Edit/>} />
            <Route path="/orders" element={<Orders/>} />
            <Route path="/allOrders" element={<Allorders/>}/>
          </Routes>
        </div>
      </Router>
    </UserProvider>
  );
}

function Home() {
  return (
    <div>
      <Login/>
    </div>
  );
}

export default App;
